import json
from copy import deepcopy

with open("groundtruth.json", "r", encoding="utf-8") as f:
    ground = f.read()

ground = json.loads(ground)

with open("myAnnotations.json", "r", encoding="utf-8") as f:
    ann = f.read()

ann = json.loads(ann)

predicted = list()
test = list()

tot = 0
perf = 0
subm = 0

keys = ground.keys()
for k in keys:
    col = ground[k].keys()
    for c in col:
        test.append(ground[k][c])
        predicted.append(ann[k][c])
        tot = tot + 1
        if(ground[k][c]==ann[k][c]):
            perf = perf + 1
        if(ann[k][c] != "undefined"):
            subm = subm +1

precision = perf / subm
recall = perf / tot
f1 = (2 * precision * recall) / (precision + recall)

uniq = deepcopy(predicted)
uniq2 = deepcopy(test)
uniq.extend(uniq2)
labl = list(set(uniq))

#print("ConfMat:")
#print(confusion_matrix(test, predicted, labels=labl))

print("Precision: " + str(precision))

print("Recall: " + str(recall))

print("f1: " + str(f1))

with open("results.txt", "w+", encoding="utf-8", newline="\n") as f:
    f.write("Precision: " + str(precision) + "\r\n")
    f.write("Recall: " + str(recall) + "\r\n")
    f.write("f1: " + str(f1))
