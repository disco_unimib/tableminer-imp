# Nodejs implementation of "Effective and efficient Semantic Table Interpretation using TabelMiner+ [1]"


## Source

ziqizhang repository: https://github.com/ziqizhang/sti

## Gold Standards

1. T2Dv2
2. Limaye2000

## Getting Started

These instructions will get you a copy of the project up and running on your local machine.

### Prerequisites

1. Download the Node.js installer for your platform at [Nodejs.org](https://nodejs.org/en/download/)
2. Download the Meteor installer at [Meteor.com](https://www.meteor.com/install)
3. Install the [Iron Command Line tool](https://github.com/iron-meteor/iron-cli) globally so you can use it from any project directory

### Installing

```
$ git clone https://github.com/iron-meteor/iron-cli.git
$ cd app
$ npm install -g
```

## Running

From the root directory of the project, launch the following command:

```
$ iron run
```

Open the web browser and go to <http://localhost:3000/> for testing the tool.

## Validation

After having exported the annotations, you can validate them by using the python script "results/results.py".
It will generate a "result.txt" file with precision, recall and f1 measures inside "result" folder.



[1] Zhang, Z. 2017. Effective and efficient semantic table interpretation using tableminer+. Semantic Web 8 (6), 921-957
