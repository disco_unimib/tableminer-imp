import {
  sparqlGetEntities,
  sparqlGetType
} from '../utils/sparqlProxy';
import {
  getRowContext,
  getHeaderContextFromHeaderTable,
  getBowSet,
  getNameFromEntity
} from '../utils/bag-of-words';
import {
  getScores
} from '../utils/scores';
import { bowText, bowSetText } from '../utils/bag-of-words';

const numberOfRows = 30;
const fs = require('fs');
const ontologyClass = new Map(JSON.parse(fs.readFileSync('assets/app/ontologyClass.json')));
const ontologyGraph = JSON.parse(fs.readFileSync('assets/app/OntologyGraph.json'));
const ontologyGraphCT = JSON.parse(fs.readFileSync('assets/app/OntologyGraphCT.json'));
let debug;

export default function getEntities(tableID, debugIsOn, chl) {
  Annotations.remove({ tableID: tableID });
  Alerts.remove({ tableID: tableID });
  debug = debugIsOn;
  // Get infos about the table
  const infoTable = InfoTable.findOne({ _id: tableID });
  const tdc = TableDataCopy.findOne({_id:tableID});
  const cols = tdc.cols;
  const header = tdc.header;
  let neCols = infoTable.neCols;
  const newNoAnnColsIndex = [];
  const subCol = infoTable.subCol;
  const totalRowsTable = infoTable.stats.row;
  let headerContext = getHeaderContextFromHeaderTable(header);
  let indexes = getIndexesRows(totalRowsTable);

  let weCols = {};
  neCols.forEach((neCol) => {
    let winningEntities = [];
    getEntitiesAndDisambiguation(cols, indexes, neCol.index, headerContext, winningEntities, tableID);
    let noAnnotation = checkIsNoAnnotation(winningEntities);
    noAnnotation = false;
    if(!chl && noAnnotation && neCol.index != subCol) {
      newNoAnnColsIndex.push(neCol.index);
    }
    else {
      let tmp = getType(winningEntities, totalRowsTable, header[neCol.index]);
      let type = tmp[0];
      let winningConcepts = tmp[1];
      neCol.type = 'http://dbpedia.org/ontology/'+type;
      neCol.winnigConcepts = winningConcepts;
      weCols[neCol.index] = winningEntities;
      winningEntities = winningEntities.map(winningEntities => winningEntities.entity);
      if(debug) {
        console.log(type);
        console.log(winningEntities);
      }
    }
  });

  neCols = neCols.filter(item => !newNoAnnColsIndex.includes(item.index));

  InfoTable.update({_id: tableID}, {$set:{neCols: neCols}});

  TableDataCopy.update({_id: tableID}, {$set:{cols: cols}});

  Annotations.update(
    {_id:tableID},
    {
      _id:tableID,
      colsWE: weCols
    },
    {upsert: true}
  );

  const util = require('util');
  var fs = require('fs');


  //ONESENSEPERDISCOURSE BEGIN
  res = {};
  key_inds = {};
  blacklist = [];
  col_ind = 0;
  for(c in tdc["cols"])
  {
    c = tdc["cols"][c];
    row_ind = 0;
    for(cell in c)
    {
      cell = c[cell];
      if((!(row_ind in blacklist)) && "value" in cell) //if the row hasn't been concatenated yet and the key exists
      {
        if(!(cell["value"].toString() in res)) //if the key is new
        { //create the new key
          res[cell["value"].toString()] = "";
          col_ind_2 = 0;
          for(c_2 in tdc["cols"]) //and concat in the value the other cells content of the same row
          {
            c_2 = tdc["cols"][c_2];
            if(col_ind != col_ind_2 && "value" in c_2[row_ind]) //if the cell is not in the same key col and have a value
            {
              res[cell["value"].toString()] += c_2[row_ind]["value"].toString();
            }
            col_ind_2 += 1;
          }
          key_inds[cell["value"].toString()] = row_ind;
        }
        else
        { //otherwise concat in the value the other cells content of the new row
          col_ind_2 = 0;
          for(c_2 in tdc["cols"])
          {
            c_2 = tdc["cols"][c_2];
            if(col_ind != col_ind_2 && "value" in c_2[row_ind])
            {
              res[cell["value"].toString()] += c_2[row_ind]["value"].toString();
            }
            col_ind_2 += 1;
          }
          if(!(row_ind in blacklist))
          {
            blacklist.push(row_ind);
          }
          if(!(key_inds[cell["value"].toString()] in blacklist))
          {
            blacklist.push(key_inds[cell["value"].toString()]);
          }
        }
      }
      row_ind += 1;
    }
    col_ind += 1;
  }

  for (k in res)
  {
    bw = bowSetText(bowText(res[k]));
    res[k] = Object.keys(bw).length;
  }

  for(c in tdc["cols"])
  {
    c = tdc["cols"][c];
    for(cell in c)
    {
      cell = c[cell];
      if("value" in cell)
      {
        cell["ospd"] = res[cell["value"].toString()];
      }
    }
  }

  function compare(a, b){
    if (a["ospd"] > b["ospd"]) return -1;
    if (a["ospd"] < b["ospd"]) return 1;
    return 0;
  }
  for(c in tdc["cols"])
  {
    tdc["cols"][c].sort(compare);
  }


  //CONFIDENCE SCORE OF ENTITY BEGIN
  ecs = [];
  xcs = [];
  dice = [];
  ec = [];
  en = [];
  cf = [];
  for(c in tdc["cols"])
  {
    colix=c;
    c = tdc["cols"][c];
    for(cell in c)
    {
      rowix=cell;
      cell = c[cell];
      if("resultEntities" in cell)
      {
        for(ents in cell["resultEntities"])
        {
        cur_ent = cell["resultEntities"][ents]["entity"];
        concat = "";
        overlap = 0;
        for(ents in cell["resultEntities"])
        {
          ents = cell["resultEntities"][ents];
          //concat += ents["entity"]
          for(ecV in ents["ec"])
          {
            concat += ents["ec"][ecV];
          }
        }
        bow1 = bowSetText(bowText(concat));
        ecs.push(bow1);

        concat="";
        col2 = tdc["cols"][colix];
        for(c2 in col2)
        {
          c2 = col2[c2];
          if("value" in c2)
          {
            concat += c2["value"];
          }
        }
        for(col2 in tdc["cols"])
        {
          col2 = tdc["cols"][col2];
          c2 = col2[rowix];
          if("value" in c2)
          {
            concat += c2["value"];
          }
        }
        bow2 = bowSetText(bowText(concat));
        xcs.push(bow2);

        pt1 = 1 / (Object.keys(bow1).length + Object.keys(bow2).length);
        pt2 = 0;
        intersect = Object.keys(bow1).filter({}.hasOwnProperty.bind(bow2));

        for(intr in intersect)
        {
          intr = intersect[intr];
          pt2 += (cur_ent.match(new RegExp(intr, 'gi')) || []).length / cur_ent.length;
          pt2 += (concat.match(new RegExp(intr, 'gi')) || []).length / concat.length;
        }
        pt2 = pt2 * 2;
        dice.push(pt1 * pt2);

        overlap += pt1 * pt2 * concat.length;
        ec.push(overlap);

        enij = Math.sqrt(2*( Object.keys(bow1).filter({}.hasOwnProperty.bind(bow2)).length / ( Object.keys(bow1).length + Object.keys(bow2).length ) ));
        en.push(enij);

        cf.push(enij + ( overlap / (Math.sqrt(Object.keys(bow2).length)) ));
        }
      }
    }
  }


  //UPDATE PHASE BEGIN
  domainbow = {}
  domain = []
  for(c in tdc["cols"])
  {
    c = tdc["cols"][c];
    for(cell in c)
    {
      cell = c[cell];
      if("resultEntities" in cell)
      {
        for(ents in cell["resultEntities"])
        {
          ents = cell["resultEntities"][ents];
          if("type" in ents)
          {
            ecbow="";
            for(ec in ents["ec"])
            {
              ecbow += ents["ec"][ec];
              if(!ents["ec"][ec] in domain)
              {
                domain.push(ents["ec"][ec]);
              }
            }
            ecbow = bowSetText(bowText(ecbow));
            domainbow = Object.assign({}, domainbow, ecbow);
          }
        }
      }
    }
  }

  dco = [];
  for(ne in neCols)
  {
    key = [];
    function logMapElements(value, k, map) {
      key.push(k)
    }
    neCols[ne]["winnigConcepts"].forEach(logMapElements);
    coldco = [];
    for(concept in key)
    {
      concept = key[concept];
      bow1 = bowSetText(bowText(concept));
      concat = "";
      for(d in domain)
      {
        concat += domain[d];
      }
      bow2 = bowSetText(bowText(concat));

      pt1 = 1 / (Object.keys(bow1).length + Object.keys(bow2).length);
      pt2 = 0;
      intersect = Object.keys(bow1).filter({}.hasOwnProperty.bind(bow2));

      for(intr in intersect)
      {
        intr = intersect[intr];
        pt2 += (cur_ent.match(new RegExp(intr, 'gi')) || []).length / concept.length;
        pt2 += (concat.match(new RegExp(intr, 'gi')) || []).length / concat.length;
      }
      pt2 = pt2 * 2;
      dco.push(Math.sqrt(pt1 * pt2));
      coldco.push(Math.sqrt(pt1 * pt2));
    }
    neCols[ne]["dc"] = coldco;
  }
  
}

function getEntitiesAndDisambiguation(cols, indexes, colIndex, bwHeaderContext, winningEntities) {
  let column = cols[colIndex];

  for(let i=0; i<indexes.length; i++) {
    let index = indexes[i]
    let valueInCell = column[index].value;
    if(valueInCell == '')
      continue;
    let entities = sparqlGetEntities(column[index].value_old);
    let resultEntities = [];
    let bwRowContext = getRowContext(cols, index, colIndex);
    let winningEntity = getWinningEntity(valueInCell, entities, bwHeaderContext, bwRowContext, resultEntities);
    cols[colIndex][index].resultEntities = resultEntities;
    let tmp = resultEntities.filter(item => item.entity == winningEntity)[0];

    if(winningEntity)
      winningEntities.push(tmp);
  }
}

export function getWinningEntity(cell, entities, bwHeaderContext, bwRowContext, resultEntities) {
  let rowContextScoreMax = 1;
  let cellContextScoreMax = 1;

  for(let j=0; j<entities.length; j++) {
    [bowSetCell, bowSetEntity, bowSetAbstract] = getBowSet(cell, entities[j].s.value, entities[j].abstract.value);
    let entity = getNameFromEntity(entities[j].s.value);
    let scores = getScores(cell, entity, bowSetCell, bowSetEntity, bowSetAbstract, bwHeaderContext, bwRowContext, resultEntities);
    if(scores.rowContextScore > rowContextScoreMax)
      rowContextScoreMax = scores.rowContextScore;
    if(scores.cellContextScore > cellContextScoreMax)
      cellContextScoreMax = scores.cellContextScore;
    resultEntities[j].entity = entities[j].s.value;
  }

  let winningEntity;
  let max = -Infinity;
  for(let j=0; j<resultEntities.length; j++) {
    let rowContextScoreNormalized = resultEntities[j].rowContextScore/rowContextScoreMax;
    let cellContextScoreNormalized = resultEntities[j].cellContextScore/cellContextScoreMax;
    let finalScore = rowContextScoreNormalized + cellContextScoreNormalized - resultEntities[j].editDistanceScore * 5;
    if(finalScore > max) {
      max = finalScore;
      winningEntity = resultEntities[j].entity;
    }
    resultEntities[j].rowContextScoreNormalized = rowContextScoreNormalized;
    resultEntities[j].cellContextScoreNormalized = cellContextScoreNormalized;
    resultEntities[j].finalScore = finalScore;
  }
  return winningEntity;
}

function checkIsNoAnnotation(winningEntities) {
  let count = 0;
  let weMap = new Map();
  for(let we of winningEntities) {
    if(!weMap.get(we.entity) && we.editDistanceScore < 0.4)
      count++;
    weMap.set(we.entity, true);
  }
  return (count/weMap.size) < 0.50;
}

function getIndexesRows(tableRows) {
  let indexes = [];
  if(tableRows <= numberOfRows) {
    for(let i=0; i<tableRows; i++)
      indexes.push(i);
  }
  else {
    for(let i=0; i<10; i++)
      indexes.push(i);
    let tmp = Math.floor(tableRows/3) + 1;
    for(let i=tmp; i<(tmp+10); i++)
      indexes.push(i);
    tmp = tableRows - 10;
    for(let i=tmp; i<(tmp+10); i++)
      indexes.push(i);
  }
  return indexes;
}

function getType(winningEntities, totalRowsTable, headerAttribute) {
  let globalConcepts = {};
  let maxType;
  let max = -Infinity;
  for(let e of winningEntities) {
    let types = sparqlGetType(e.entity);
    e.type = types.map(type => (type.type.value));
    types = extractTypes(types);
    let localConcepts = {};
    for(let type of types) {
      //type = type.split(/[0-9]+/)[0];
      if(ontologyClass.get(type)) {
        if(!globalConcepts[type]) {
          globalConcepts[type] = {};
          globalConcepts[type].frequency = 0;
          globalConcepts[type].row = 0;
        }
        if(!localConcepts[type]) {
          localConcepts[type] = true;
          globalConcepts[type].row++;
        }
        globalConcepts[type].frequency++;
        if(globalConcepts[type].frequency > max) {
          max = globalConcepts[type].frequency;
          maxType = type;
        }
      }
    }
  }
  let winningConcepts = new Map();
  for(let key in globalConcepts) {
    let weightRows = (numberOfRows > totalRowsTable) ? totalRowsTable : numberOfRows;
    if(globalConcepts[key].row/weightRows > 0.25 && globalConcepts[key].frequency/max > 0.25) {
      winningConcepts.set(key, globalConcepts[key].frequency);
    }
  }
  if(debug) {
    console.log(globalConcepts);
    console.log(winningConcepts);
  }
  let type = findPossibleConceptInHeader(winningConcepts, headerAttribute);
  type = type ? type : findPossibleSubType(maxType, winningConcepts);
  return [type, winningConcepts];
}

function extractTypes(types) {
  let str = '';
  for(let type of types) {
      let tmp = type.type.value;
      str += tmp.substring(tmp.lastIndexOf('/')+1, tmp.length) +  ' ';
  }
  str = str.trim();
  types = str.split(' ');
  return types;
}

function findPossibleConceptInHeader(types, header) {
  header = header.split(' ');
  let keys = [...types.keys()];
  let concept = [];
  for(let h of header) {
    h = h.charAt(0).toUpperCase() + h.substring(1, h.length);
    concept = concept.concat(keys.filter(concept => concept.includes(h)));
  }
  let type
  if(concept.length > 0)
    type = concept.reduce(function (a, b) { return a.length > b.length ? a : b; });
  return type ? type : undefined;
}

function findPossibleSubType(type, types) {
  while(true) {
    let substitution = false;
    types.forEach((val, key) => {
      if(ontologyGraph[type] && isMoreSubType(type, types))
        return type;
      if(isSubType(type, key)) {
        type = key;
        substitution = true;
      }
    });
    if(!substitution)
      break;
  }
  return type;
}

function isSubType(type1, type2) {
  if(!ontologyGraph[type1])
    return false;
  else if(ontologyGraph[type1][type2] || ontologyGraphCT[type1][type2])
    return true;
  else
    return false;
}

function isMoreSubType(type, types) {
  let count = 0;
  types.forEach((val, key) => {
    if(ontologyGraph[type][key])
      count++;
  });
  return (count > 1);
}
