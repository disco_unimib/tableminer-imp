import nlp from 'wink-nlp-utils';
import { googleWebSearch } from '../../utils/web-search';
import { bowText, bowSetText, getSingular, getPlural } from '../../utils/bag-of-words';
import { Promise } from "meteor/promise";
/*
 * For every column => number of 'empty' / number of rows
 * @param fileid
 * @returns {number}
 */
function computeEMC(tableID, col, numRows) {
  let dataType = TableDataCopy.findOne({_id: tableID}).colType[col];
  var emptyCells = 0;
  for( var k in dataType) {
    if (k == 'empty') {
      emptyCells = dataType[k];
    }
  }
  return parseFloat(emptyCells / numRows);
}

function computeCM(tableID, tabl, colIndex) {
	let table = TableDataCopy.findOne({_id: tableID});
  if(table.typeGS == "TD2v2"){
    let tokenHeader = table.header;
    let context = table.context;
    index = context["webPageTitle"].lastIndexOf('/')
    text = context["webPageTitle"].substring(index)
    let bowWebPageTitle = bowSetText(bowText(text.toLowerCase()));
    //console.log(bowWebPageTitle)
    let bowTableTitle = bowSetText(bowText(context["tableTitle"].toLowerCase()));
    //console.log(bowTableTitle)
    let bowParagraphs = bowSetText(bowText(context["paragraphs"].toLowerCase()));
    //console.log(bowParagraphs)
    sum = 0;
    wWeb = 1;
    wTab = 1;
    wPar = 0.5;
    for(token of tokenHeader){
      if(bowWebPageTitle[getSingular(token)] != undefined){
        sum += bowWebPageTitle[getSingular(token)] * wWeb;
        //console.log(bowWebPageTitle[getSingular(token)])
      }
      else if(bowWebPageTitle[getPlural(token)] != undefined){
        sum += bowWebPageTitle[getPlural(token)] * wWeb;
      }
      if(bowTableTitle[getSingular(token)] != undefined){
        sum += bowTableTitle[getSingular(token)] * wTab;
      }
      else if(bowTableTitle[getPlural(token)] != undefined){
        sum += bowTableTitle[getPlural(token)] * wTab;
      }
      if(bowParagraphs[getSingular(token)] != undefined){
        sum += bowParagraphs[getSingular(token)] * wPar;
      }
      else if(bowParagraphs[getPlural(token)] != undefined){
        sum += bowParagraphs[getPlural(token)] * wPar;
      }
    }
    //console.log(sum);
    return sum;
  }
  else{
    return 0;
  }
}

function computeAC(fileid, table, col) {
  var res = 0
  if("litCols" in table && table["litCols"].length>0 && "colType" in table["litCols"][0] && table["litCols"][0]["colType"].length>0){
      for (var ind = 0; ind < table["litCols"][0]["colType"].length; ind++){
          if("name" in table["litCols"][0]["colType"][ind] && table["litCols"][0]["colType"][ind]["name"] == "ID"){
              res = 1
              break;
          }
      }
  }
  return res;
}

/**
 * It counts, taken an element, how many times it occurs
 * @param fileid
 * @returns {number}
 */
export function computeUC(fileid, numRows, col) {
  const uniqueCells = Promise.await(TableDataCopy.aggregate([
      {$match: {_id: fileid}},
      {
        $project:
         {
            _id: 0,
            first: { $arrayElemAt: [ "$cols", col ] }
         }
      },
      {$unwind: "$first" },
      {$group:
          {
            _id: '$first.value',
            count: { $sum: 1 }
          }
      }
   ]).toArray());
  // Number of groups for every column / number of rows
  return Object.keys(uniqueCells).length / numRows;
}

function computeWS(fileid, col, colIndex, litCols) {
    var litHeaders = '';
    for(var litCol of litCols) {
      litHeaders += litCol.header.toLowerCase() + ' ';
    }
    var query = col.toLowerCase() +' '+litHeaders;
    var webSearchText = googleWebSearch(query);

    var text = '';
    for(var tmp of webSearchText)
      text += tmp.title.toLowerCase() + ' ' + tmp.snippet.toLowerCase();
    text = text.split('.').join('');
    bw = bowSetText(bowText(text));
  var token = col.split(' ');
  var tmp = '';
  var wsScore = 0;
  for(var t of token) {
    tmp = t.toLowerCase();
    wsScore += bw[tmp] ? bw[tmp] : 0;
  }
  return wsScore;
}


export default function getSubCol(fileid) {
  const table = InfoTable.findOne({ _id: fileid });
  // Initializing scores
  var emc = [];
  var uc = [];
  var df = [];
  var ac = [];
  var ws = [];
  var cm = [];
  var finalScore = [];

  Alerts.insert({ tableID: fileid,
    state: 'columnsAnalysis',
    value: 'Determinate subject column' });

  var score = [];
  var firstNE = 0;
  var neCols = table.neCols;
  for (var i = 0; i < neCols.length; i++) {
    var element = {};
    var col = neCols[i].header;
    var colIndex = neCols[i].index;
    if (i == 0)
      firstNE = colIndex;
    // EMC
    emc.push(computeEMC(fileid, colIndex, table.numRows).toFixed(3));
    element.emc = parseFloat(emc[i]);
    // CM
    cm.push(computeCM(fileid, table, colIndex));
	element.cm = parseFloat(cm[i]);
    // AC
    ac.push(computeAC(fileid, table, col));
    element.ac = ac[i];
    // UC
    uc.push(computeUC(fileid, table.numRows, colIndex).toFixed(1));
    element.uc = parseFloat(uc[i]);
    // DF: distance from the column analyzed and the first NE column
    df.push(colIndex - firstNE);
    element.df = df[i];
    // WS
    var wsScore = computeWS(fileid, col, colIndex, table.litCols);
    if(wsScore < 5)
      wsScore += 5;
    ws.push(wsScore);
    element.ws = ws[i];
    score.push(element);
  }

  for (var i = 0; i < table.neCols.length; i++) {
    var emcNorm = 0;
    if (emc[i] != 0) {
      emcNorm = emc[i] / Math.max(...emc);
    }
    var wsNorm = ws[i] / Math.max(...ws);
    var ucNorm = uc[i] / Math.max(...uc);
	var cmNorm=0;
	if(Math.max(...cm)>0)
		cmNorm = cm[i] / Math.max(...cm);
    var dfFinal = df[i] + 1;
    if(table.neCols.length < 3)
      weightWS = 2;
    else
      weightWS = 0;
    finalScore.push((ucNorm + 2*cmNorm + 2*wsNorm - emcNorm) / Math.sqrt(dfFinal));
    element = score[i];
    element.emcNorm = emcNorm.toFixed(1);
    element.ucNorm = ucNorm.toFixed(1);
    element.dfFinal = dfFinal;
	element.cmFinal = cmNorm;
    element.wsNorm = wsNorm.toFixed(1);
    element.finalScore = finalScore[i].toFixed(2);
  }

  var indexSubCol = finalScore.indexOf(Math.max(...finalScore));
  var neColsIndex = -1; //value for index subCol for table without subject column
  if(indexSubCol != -1)
    neColsIndex = neCols[indexSubCol].index;

  Alerts.insert({
    tableID: fileid,
    state: 'columnsAnalysis',
    value: `The subject column is the column with index: ${neColsIndex}`
  });

  for(var i=0; i<neCols.length; i++)
    neCols[i].score = score[i];

  InfoTable.update({ _id: fileid }, { $set: { 'subCol': neColsIndex, 'neCols': neCols } });
}
