import validator from 'validator';
import nlp from 'wink-nlp-utils';
const isValidDate = require('is-valid-date');
const isDate = require('is-datestring');

export default function checkType(value,currency,iata) {
  const oldValue = value;
  let newValue = value.toLowerCase();

  if (validator.isEmpty(newValue) || nlp.string.retainAlphaNums(newValue) === '') {
    return 'empty';
  }

  let date = newValue.match(/[0-9]+-[0-9]+-[0-9]+/);
  if(date && (newValue.length - date[0].length) <= 19) {
      return 'date';
  }

  if (validator.isNumeric(newValue.replace(/,/g, '')) || newValue.match(/[0-9],[0-9]+ ft | [0-9]+ m/) != null || newValue.match(/[0-9]+ m/) != null)
    return 'numeric';

  //if (validator.isDecimal(newValue.replace(/,/g, ''))) return 'decimal';
  //if (validator.isInt(newValue.replace(/,/g, ''))) return 'integer';
  if (isDate(newValue)) return 'date';
  if (isValidDate(newValue)) return 'date';
  if(oldValue.length > 80) return 'description';

let numsOfCommas = value.split(',').length;
if(numsOfCommas > 1)
  return 'NoAnnotation';
  // Returns an empty strings if it can't find the right type
  return '';
}
