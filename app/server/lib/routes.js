Router.route('downloadMyAnnotations', {
  name: 'download',
  where: 'server',
  subscriptions() {
    this.subscribe('infoTable');
  },
  action() {
    const fromMongo = InfoTable.find().fetch();
    let json = {};
    for (let i = 0; i < fromMongo.length; i++){
      for (let j = 0; j < fromMongo[i].neCols.length; j++){
        if(fromMongo[i].neCols[j].type != undefined){
          index = fromMongo[i].neCols[j].type.lastIndexOf('/')+1;
          text = fromMongo[i].neCols[j].type.substring(index);
        }
        else{
          text = "undefined";
        }
        if (!(fromMongo[i]._id in json)){
          json[fromMongo[i]._id]={};
        }
        json[fromMongo[i]._id][fromMongo[i].neCols[j].index.toString()] = text;
      }
    }
    const headers = {
      'Content-Type': 'text/json',
      'Content-Disposition': 'attachment; filename=myAnnotations.json',
    };
    this.response.writeHead(200, headers);
    return this.response.end(JSON.stringify(json, null, 2));
  },
});
